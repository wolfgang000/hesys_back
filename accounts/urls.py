from django.urls import path, include, re_path
from django.conf import settings
from . import views

urlpatterns = [
	path('login/', views.Login.as_view(), name='login'),
	path('logout/', views.Logout.as_view(), name='logout'),
	path('token/', include('knox.urls')),
]