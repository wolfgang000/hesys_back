from django.shortcuts import render
from django.contrib import auth
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import status
from rest_framework.views import APIView

from django.contrib.auth import get_user_model
User = get_user_model()
from . import models, serializers

from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie
from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters


class Login(APIView):
	permission_classes = (AllowAny,)

	def post(self, request,):
		serializer = serializers.Login(data=request.data)
		if serializer.is_valid():
			email = serializer.validated_data['username'].lower()
			user = auth.authenticate(
				request, 
				username = email, 
				password = serializer.validated_data['password']
			)
			if user is not None:
				auth.login(request, user)
				return Response(status=status.HTTP_200_OK)
			return Response(status=status.HTTP_400_BAD_REQUEST, )
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	@method_decorator(ensure_csrf_cookie)
	def get(self, request,):
		return Response(status=status.HTTP_200_OK)

class Logout(APIView):
	permission_classes = (AllowAny,)

	def get(self, request,):
		auth.logout(request)
		return Response(status=status.HTTP_200_OK)
