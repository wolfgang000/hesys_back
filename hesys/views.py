from . import models
import datetime
from django.shortcuts import render
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import pagination
from rest_framework.response import Response
from rest_framework.settings import api_settings

from django.urls import reverse
from django.http import Http404


import csv
from django.http import HttpResponse


class DataLogCsvStats(APIView):
	
    def get(self, request,):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
        writer = csv.writer(response)
        writer.writerow(['datetime', 'i1', 'i2', 'i3'])

        timeframe = request.query_params.get("filter_by", "last_24_hours")
        if timeframe == 'last_24_hours':
            date_from = datetime.datetime.now() - datetime.timedelta(days=1)
            stats = models.DataLog.objects.filter(created_at__gte=date_from)

            for instance in stats:
                writer.writerow([instance.created_at, instance.i1, instance.i2, instance.i3 ])
            return response

        elif timeframe == 'last_48_hours':
            date_from = datetime.datetime.now() - datetime.timedelta(days=2)
            data = models.DataLog.objects.filter(created_at__gte=date_from)
            for instance in stats:
                writer.writerow([instance.created_at, instance.i1, instance.i2, instance.i3 ])

            return response

        else:
            return Response("Error in timeframe.", status=status.HTTP_400_BAD_REQUEST)