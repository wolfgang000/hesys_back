from channels.generic.websocket import WebsocketConsumer
import json
from .state import state
from . import serializers

class ControlConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()
        serializer = serializers.State(state)
        self.send(text_data=json.dumps(serializer.data))

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        json_message = json.loads(text_data)
        
        if json_message.get('pump_power') is not None:
            state.pump_power = json_message.get('pump_power') 

        if json_message.get('setpoint') is not None:
            state.setpoint = json_message.get('setpoint') 

        serializer = serializers.State(state)
        self.send(text_data=json.dumps(serializer.data))