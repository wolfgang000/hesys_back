from django.apps import AppConfig


class HesysConfig(AppConfig):
    name = 'hesys'
