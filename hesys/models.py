from django.db import models
from django.utils import timezone
from datetime import timedelta
from django.conf import settings

class DataLog(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    i1 = models.FloatField()
    i2 = models.FloatField()
    i3 = models.FloatField()
    bot_temp = models.FloatField()
    surface_temp = models.FloatField()

