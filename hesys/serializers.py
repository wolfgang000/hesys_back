from rest_framework import serializers
from django.contrib.auth import password_validation
from django.core import exceptions
from . import models
from django.contrib.auth import get_user_model


class State(serializers.Serializer):
	pump_power = serializers.BooleanField()
	operation_status = serializers.BooleanField()
	setpoint = serializers.FloatField()