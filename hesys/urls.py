from django.urls import path
from django.conf import settings
from . import views

urlpatterns = [
    path('csvlog/', views.DataLogCsvStats.as_view(), name='data_csv'),
]

