from django.conf import settings


if settings.ENVIRONMENT == 'developmentPC':
    from hesys_back.micro_fake import values as rp_values
else:
    from  hesys_back.micro import values as rp_values


class State(object):

	def __init__(self,):
		self.pump_power = False


	## Pump Power
	@property
	def pump_power(self):
		return self.__pump_power

	@pump_power.setter
	def pump_power(self, value):
		if value is True:
			self.__pump_power = start = True
		else:
			self.__pump_power = stop = False

	## Operation_status
	@property
	def operation_status(self):
		return rp_values[12]

	## setpoint
	@property
	def setpoint(self):
		return rp_values[2]
	
	@setpoint.setter
	def setpoint(self, value):
		rp_values[2] = value


state = State()