import RPi.GPIO as GPIO
import spidev
import time
##########################
spipic = spidev.SpiDev()                                                #SPI DAC
spipic.open(0,0)
spipic.max_speed_hz = 500
spipic.mode = 1
###############################
dummy = [1]*12
values = [0]*8
values.append(False)
values.append(False)
values.append(False)
values.append(False)
values.append(False)

import asyncio

async def read_data():
    loop = asyncio.get_running_loop()
    while True:
        data = spipic.xfer([1])
        if data[0] == 170:
            data = spipic.xfer(dummy)
            buffer = data[0]
            buffer = buffer << 8
            buffer = buffer + data[1]
            values[0] = buffer
            buffer = data[2]
            buffer = buffer << 8
            buffer = buffer + data[3]
            values[1] = buffer
            buffer = data[4]
            buffer = buffer << 8
            buffer = buffer + data[5]
            values[2] = buffer
            buffer = data[6]
            values[3] = buffer
            buffer = data[7]
            values[4] = buffer
            buffer = data[8]
            values[5] = buffer
            buffer = data[9]
            values[6] = buffer
            buffer = data[10]
            values[7] = buffer
            buffer = data[11]
            if(buffer & 0b01):
                values[8] = True
            else:
                values[8] = False
            if(buffer & 0b010):
                values[9] = True
            else:
                values[9] = False
            if(buffer & 0b0100):
                values[10] = True
            else:
                values[10] = False
            if(buffer & 0b01000):
                values[11] = True
            else:
                values[11] = False
            if(buffer & 0b010000):
                values[12] = True
            else:
                values[12] = False

            print(values)
        await asyncio.sleep(1)